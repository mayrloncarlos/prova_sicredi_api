package br.com.sicred.viacepautomation.core;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.util.ArrayList;

public class BaseAction {

    protected static ThreadLocal<Response> response = new ThreadLocal<>();

    /**
     * Metodo que captura o valor de uma coluna da requisicao.
     *
     * @param value coluna a ser retornado o valor
     * @return retorna o valor da coluna
     */
    protected String getJsonValue(String value) {
        if (response.get().jsonPath().getString(value) == null) {
            return "";
        } else {
            return response.get().jsonPath().getString(value);
        }
    }

    /**
     * Metodo que captura o codigo de retorno da requisicao.
     *
     * @return retorna o codigo
     */
    protected int getStatusCode() {
        return response.get().statusCode();
    }

    /**
     * Metodo que captura o valor do array de um json.
     *
     * @param index index a ser capturado o valor
     * @param element elemento do array
     * @return retorna valor do index do array
     */
    protected String getValueFromJsonArray(int index, String element) {
        ArrayList<?> json = response.get().path(element);

        if (json.get(index) == null) {
            return "";
        } else {
            return json.get(index).toString();
        }
    }

    /**
     * Metodo que realiza uma requisicao do tipo GET.
     *
     * @param cep        parametro cep da requisicao
     */
    public void requestGetWithCep(String cep) {
        response.set(RestAssured.given()
                .when()
                .contentType("application/json")
                .get("https://viacep.com.br/ws/"+cep+"/json/")
                .then()
                .extract()
                .response());
    }

    /**
     * Metodo que realiza uma requisicao do tipo GET.
     *
     * @param uf         parametro uf da requisicao
     * @param localidade parametro localidade da requisicao
     * @param logradouro parametro logradouro da requisicao
     */
    public void requestGetWithUfAndLocalidadeAndLogradouro(String uf, String localidade, String logradouro) {
        response.set(RestAssured.given()
                .when()
                .contentType("application/json")
                .get("https://viacep.com.br/ws/"+uf+"/"+localidade+"/"+logradouro+"/json/")
                .then()
                .extract()
                .response());
    }

}
