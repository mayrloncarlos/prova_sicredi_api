package br.com.sicred.viacepautomation.actions;

import br.com.sicred.viacepautomation.core.BaseAction;

public class ConsultaCepAction extends BaseAction {

    protected String cep;
    protected String uf;
    protected String localidade;
    protected String logradouro;
}
