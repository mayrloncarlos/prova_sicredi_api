package br.com.sicred.viacepautomation.validacoes;

import br.com.sicred.viacepautomation.actions.ConsultaCepAction;

import static org.testng.Assert.assertEquals;

public class ConsultaCepAsserts extends ConsultaCepAction {

    public void validarBodySucesso(){
        assertEquals(getJsonValue("cep"),"58062-112");
        assertEquals(getJsonValue("logradouro"), "Rua Fernando José da Silva Ferreira");
        assertEquals(getJsonValue("complemento"),"(Lot Paratibe)");
        assertEquals(getJsonValue("bairro"),"Paratibe");
        assertEquals(getJsonValue("localidade"),"João Pessoa");
        assertEquals(getJsonValue("uf"),"PB");
        assertEquals(getJsonValue("ibge"),"2507507");
        assertEquals(getJsonValue("gia"),"");
        assertEquals(getJsonValue("ddd"),"83");
        assertEquals(getJsonValue("siafi"),"2051");
    }

    public void validarBodyAtributoErro(){
        assertEquals(getJsonValue("erro"),"true");
    }

    public void validarMensagemErro(){
        if(getStatusCode() == 400){
            assertEquals(getJsonValue("message"), "Verifique a sua URL (Bad Request)");
        }
    }

    public void validarBodyMultiplosCeps(){
        assertEquals(getValueFromJsonArray(0,"cep"), "94085-170");
        assertEquals(getValueFromJsonArray(0,"logradouro"), "Rua Ari Barroso");
        assertEquals(getValueFromJsonArray(0,"complemento"), "");
        assertEquals(getValueFromJsonArray(0,"bairro"), "Morada do Vale I");
        assertEquals(getValueFromJsonArray(0,"localidade"), "Gravataí");
        assertEquals(getValueFromJsonArray(0,"uf"), "RS");
        assertEquals(getValueFromJsonArray(0,"ibge"), "4309209");
        assertEquals(getValueFromJsonArray(0,"gia"), "");
        assertEquals(getValueFromJsonArray(0,"ddd"), "51");
        assertEquals(getValueFromJsonArray(0,"siafi"), "8683");

        assertEquals(getValueFromJsonArray(1,"cep"), "94175-000");
        assertEquals(getValueFromJsonArray(1,"logradouro"), "Rua Almirante Barroso");
        assertEquals(getValueFromJsonArray(1,"complemento"), "");
        assertEquals(getValueFromJsonArray(1,"bairro"), "Recanto Corcunda");
        assertEquals(getValueFromJsonArray(1,"localidade"), "Gravataí");
        assertEquals(getValueFromJsonArray(1,"uf"), "RS");
        assertEquals(getValueFromJsonArray(1,"ibge"), "4309209");
        assertEquals(getValueFromJsonArray(1,"gia"), "");
        assertEquals(getValueFromJsonArray(1,"ddd"), "51");
        assertEquals(getValueFromJsonArray(1,"siafi"), "8683");
    }
}
