package br.com.sicred.viacepautomation.steps.consultacep;

import br.com.sicred.viacepautomation.actions.ConsultaCepAction;
import br.com.sicred.viacepautomation.validacoes.ConsultaCepAsserts;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import io.restassured.RestAssured;

public class ConsultaCepSteps extends ConsultaCepAction {

    @Before
    public void setUp(){
        RestAssured.useRelaxedHTTPSValidation();
    }

    private ConsultaCepAsserts consultaCepAsserts = new ConsultaCepAsserts();

    // --------------------------------------------------------- DADO --------------------------------------------------------//
    @Dado("que o usuário insere um CEP válido")
    public void queOUsuarioInsereUmCepValido(){
        cep = "58062112";
    }

    @Dado("que o usuário insere um CEP que não exista na base dos Correios")
    public void queOUsuarioInsereUmCepQueNaoExistaNaBaseDosCorreios(){
        cep= "11111111";
    }

    @Dado("que o usuário insere um CEP com formato inválido")
    public void queOUsuarioInsereUmCepComFormatoInvalido(){
        cep= "aaaaaaaa";
    }

    @Dado("que o usuário insere um CEP informando UF, Localidade e Logradouro")
    public void queOUsuarioInsereUmCepInformandoUfLocalidadeELogradouro(){
        uf= "RS";
        localidade= "Gravatai";
        logradouro= "Barroso";
    }

    // --------------------------------------------------------- QUANDO --------------------------------------------------------//
    @Quando("o serviço é consultado")
    public void oServicoEhConsultado(){
        if(cep == null){
            requestGetWithUfAndLocalidadeAndLogradouro(uf,localidade,logradouro);
        } else{
            requestGetWithCep(cep);
        }
    }
    // --------------------------------------------------------- ENTAO --------------------------------------------------------//
    @Então("é retornado o CEP, logradouro, complemento, bairro, localidade, uf e ibge.")
    public void EhRetornadoOsDadosDoBody(){
        consultaCepAsserts.validarBodySucesso();
    }

    @Então("é retornada um atributo erro")
    public void EhRetornadaUmAtributoErro(){
        consultaCepAsserts.validarBodyAtributoErro();
    }

    @Então("é retornado uma mensagem de erro")
    public void EhRetornadoUmaMensagemDeErro(){
        consultaCepAsserts.validarMensagemErro();
    }

    @Então("é retornado todos os CEPS disponíveis")
    public void EhRetornadoTodosOsCepsDisponiveis(){
        consultaCepAsserts.validarBodyMultiplosCeps();
    }
}
