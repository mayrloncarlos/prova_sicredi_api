#  language: pt

@AllCenarios
Funcionalidade: Consulta de CEP

  Cenário: Consulta CEP valido
    Dado que o usuário insere um CEP válido
    Quando o serviço é consultado
    Então é retornado o CEP, logradouro, complemento, bairro, localidade, uf e ibge.

  Cenário: Consulta CEP inexistente
    Dado que o usuário insere um CEP que não exista na base dos Correios
    Quando o serviço é consultado
    Então é retornada um atributo erro

  Cenário: Consulta CEP com formato inválido
    Dado que o usuário insere um CEP com formato inválido
    Quando o serviço é consultado
    Então é retornado uma mensagem de erro

  Cenário: Consulta CEP com informações adicionais
    Dado que o usuário insere um CEP informando UF, Localidade e Logradouro
    Quando o serviço é consultado
    Então é retornado todos os CEPS disponíveis
