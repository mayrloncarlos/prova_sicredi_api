package br.com.sicred.viacepautomation.runners;

import br.com.sicred.viacepautomation.core.BaseTestCase;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;

@CucumberOptions(tags = {"@AllCenarios"},
        features = "src/test/java/br/com/sicred/viacepautomation/features/consultacep.feature",
        glue = {"br.com.sicred.viacepautomation.steps.consultacep"},
        snippets = SnippetType.CAMELCASE)
public class ConsultaCepRunner extends BaseTestCase {

}
